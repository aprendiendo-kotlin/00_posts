package com.naposystems.testposts.entities

/**
 *
 * @property id Long
 * @property userId Long
 * @property title String
 * @property body String
 * @constructor
 */
data class Post(
    val id: Long,
    val userId: Long,
    val title: String,
    val body: String
)