package com.naposystems.testposts.ui.acerca

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.naposystems.testposts.R
import com.naposystems.testposts.databinding.FragmentAcercaBinding

/**
 *
 * @property binding FragmentAcercaBinding
 */
class AcercaFragment : Fragment() {

    lateinit var binding: FragmentAcercaBinding

    /**
     *
     * @param inflater LayoutInflater
     * @param container ViewGroup?
     * @param savedInstanceState Bundle?
     * @return View?
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_acerca, container, false)

        return binding.root

    }
}