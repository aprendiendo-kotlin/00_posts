package com.naposystems.testposts.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.naposystems.testposts.R
import com.naposystems.testposts.databinding.FragmentHomeBinding

/**
 *
 * @property viewModel HomeViewModel
 * @property viewModelFactory HomeViewModelFactory
 * @property binding FragmentHomeBinding
 */
class HomeFragment : Fragment() {

    private lateinit var viewModel: HomeViewModel

    private lateinit var viewModelFactory: HomeViewModelFactory

    private lateinit var binding: FragmentHomeBinding

    /**
     *
     * @param inflater LayoutInflater
     * @param container ViewGroup?
     * @param savedInstanceState Bundle?
     * @return View?
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)

        viewModelFactory = HomeViewModelFactory()

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)

        binding.homeViewModel = viewModel

        return binding.root

    }

}