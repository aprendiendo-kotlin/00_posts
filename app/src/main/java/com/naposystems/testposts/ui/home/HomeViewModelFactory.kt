package com.naposystems.testposts.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

/**
 * ViewModelFactory
 */
class HomeViewModelFactory : ViewModelProvider.Factory {

    /**
     * @param modelClass Class<T>
     * @return T
     */
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        if (modelClass.isAssignableFrom(HomeViewModel::class.java)) {

            return HomeViewModel() as T

        }

        throw IllegalArgumentException("Unknown ViewModel class")

    }

}