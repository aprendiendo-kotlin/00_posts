package com.naposystems.testposts.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.naposystems.testposts.entities.Post

/**
 *ViewModel del fragmento
 *
 * @property _postAux List<Post>
 * @property _posts MutableLiveData<List<Post>>
 * @property posts LiveData<List<Post>>
 * @property _cantidadPosts MutableLiveData<String>
 * @property cantidadPosts LiveData<String>
 */
class HomeViewModel : ViewModel() {

    private val _postAux: List<Post> = listOf(
        Post(1, 1, "Hola", "Texto"),
        Post(2, 2, "Hola2", "Texto2"),
        Post(3, 3, "Hola3", "Texto3"),
        Post(4, 4, "Hola4", "Texto4"),
        Post(5, 5, "Hola5", "Texto5")
    )

    private var _posts: MutableLiveData<List<Post>>

    private val posts: LiveData<List<Post>>
        get() = _posts

    // The current score
    private val _cantidadPosts = MutableLiveData<String>()

    val cantidadPosts: LiveData<String>
        get() = _cantidadPosts

    init {

        _posts = MutableLiveData<List<Post>>().apply {
            value = _postAux
        }

        _cantidadPosts.value = _posts.value?.size.toString()
    }

}