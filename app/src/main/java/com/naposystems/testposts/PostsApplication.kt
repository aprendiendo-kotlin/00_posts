package com.naposystems.testposts

import android.app.Application
import timber.log.Timber

/**
 *
 */
class PostsApplication : Application() {

    /**
     *
     */
    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }

}